from Alunos.Models import Alunos

def AtualizarAluno(ra, data):

    for item in Alunos.Alunos:
        if item['ra'] == ra:

            item['nome'] = data['nome']
            item['ra'] = data['ra']
            item['status'] = data['status']

            return item
    return False