from Alunos.Models import Alunos
from Alunos.Services import EncontrarAluno, MakeResponse

def CadastrarAluno(data):
    aluno = EncontrarAluno.EncontrarAluno(data['ra'])

    if aluno:
        MakeResponse.Response('Erro', 'Aluno já existe', '')
        return 403
    else:
        Alunos.Alunos.append(data)
        MakeResponse.Response('Sucesso', 'Aluno Cadastrado', data)
        return 200