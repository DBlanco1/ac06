from Alunos.Server import App
from Alunos.Models import Resposta
from Alunos.Services import MakeResponse
from flask import jsonify

@App.errorhandler(404)
def TrataNaoEncontrado(error):
    MakeResponse.Response(404, "Erro {0}.".format(error), '')
    return (jsonify(Resposta.Resposta)), 404

@App.errorhandler(500)
def ErroInterno(error):
    MakeResponse.Response(500, "Erro {0}.".format(error), '')
    return (jsonify(Resposta.Resposta)), 500

@App.errorhandler(403)
def proibido(error):
    MakeResponse.Response(403, "Erro {0}.".format(error), '')
    return (jsonify(Resposta.Resposta)), 403

@App.errorhandler(400)
def invalido(error):
    MakeResponse.Response(400, "Erro {0}.".format(error), '')
    return (jsonify(Resposta.Resposta)), 400