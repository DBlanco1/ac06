from flask import Flask

App = Flask(__name__)

from Alunos.Routes import *
from Alunos.ErrorHandler import *

if __name__ == "__main__":
    App.run(port=8080)