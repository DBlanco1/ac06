from Alunos.Server import App
from Alunos.Services import Listar, EncontrarAluno, DeletarAluno, AtualizarAluno, MakeResponse, Cadastrar
from flask import jsonify, request, json
from Alunos.Models import Resposta, Alunos

@App.route("/Alunos", methods=["GET", "POST"])
def ListarRoute():
    if request.method == "GET":

        lista = Listar.Listar()

        if len(lista) > 0:
            MakeResponse.Response('Sucesso', 'Listagem de alunos', lista)
            return (jsonify(Resposta.Resposta)), 200
        else:
            MakeResponse.Response('Sucesso', 'Nenhum aluno cadastrado', '')
            return (jsonify(Resposta.Resposta)), 404

    elif request.method == "POST":

        data = request.data
        data = json.loads(data)

        status = Cadastrar.CadastrarAluno(data)

        return jsonify(Resposta.Resposta), status

@App.route("/Alunos/<ra>", methods=["GET", "PUT", "DELETE"])
def getByRa(ra):

    if request.method == "GET":

        aluno = EncontrarAluno.EncontrarAluno(ra)

        if aluno:
            MakeResponse.Response('Sucesso', 'Aluno encontrado', aluno)
            return jsonify(Resposta.Resposta), 200
        else:
            MakeResponse.Response('Erro', 'Aluno nao encontrado', '')
            return jsonify(Resposta.Resposta), 404

    elif request.method == "DELETE":

        aluno = DeletarAluno.DeletarAluno(ra)

        if aluno:
            MakeResponse.Response('Sucesso', 'Aluno Excluido', '')
            return jsonify(Resposta.Resposta), 200
        else:
            MakeResponse.Response('Error', 'Aluno não encontrado', '')
            return jsonify(Resposta.Resposta), 404

    elif request.method == "PUT":

        data = request.data
        data = json.loads(data)

        aluno = AtualizarAluno.AtualizarAluno(ra, data)

        if aluno:
            MakeResponse.Response('Sucesso', 'Aluno atualizado', aluno)
            return jsonify(Resposta.Resposta), 200
        else:
            MakeResponse.Response('Error', 'Aluno nao encontrado', '')
            return jsonify(Resposta.Resposta), 404
